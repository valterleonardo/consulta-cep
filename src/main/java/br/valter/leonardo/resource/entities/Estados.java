package br.valter.leonardo.resource.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Estados {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "seq_estados")
	private Integer id;
	private String uf;
	
	public Estados() {}
	public Estados(String uf) {
		this.setUf(uf); 
	}
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
}